all:
	g++ main.cc -c -o main.o
	g++ main.o -lsfml-window -lsfml-graphics -lsfml-system -o tetris
	xterm -fn  -schumacher-clean-bold-r-normal--10-100-75-75-c-60-iso646.1991-irv -e "./tetris;read"
