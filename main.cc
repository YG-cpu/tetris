#include <ctime>
#include <SFML/Graphics.hpp>

int main(int argc, char** argv)
{
  sf::RenderWindow window(sf::VideoMode(600, 600), "Tetris");

  sf::Texture tile_t;
  tile_t.loadFromFile("tile.png");
  sf::Sprite tile(tile_t);

  sf::Event event;
  while (window.isOpen())
  {
    while (window.pollEvent(event))
    {
      if (event.type == sf::Event::EventType::Closed) 
        window.close();
    }
    window.clear(sf::Color::Magenta);
    window.draw(tile);
    window.display();
  }
  return 0;
}